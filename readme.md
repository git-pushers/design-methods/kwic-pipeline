### Problem A. Key Word in Context (KWIC)
Method 3. Pipes-and-filters

### Example
```shell
javac Main.java
java Main.java text.text
```
`#` - is a page separator

### Сomparison
| Criteria/Solution                   | Pipes-and-filters (This one)                                                                                                          | Main/Subroutine with stepwise refinement/ Shared data (Alternative)                                                      |
|-------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| Change the implementation algorithm | + Easy to turn on/off one of the filters.  For ex. turning on sorting. Some changes can be difficult, if it affects pipeline message. | +- Easy to make changes in shared structure,  but there can be uncontrolled behavior  because modules are not isolated.  |
| Change data representation          | - It can be difficult to do,  because pipeline message is common                                                                      | - It can be difficult to do, because  shared structure is common                                                         |
| Perfomance                          | No difference                                                                                                                         | No difference                                                                                                            |
| Reuse                               | KWIC can be easily decompose into sequence of  filters for pipeline pattern. It is good to have declarative  description of algorithm |                                                                                                                          |


