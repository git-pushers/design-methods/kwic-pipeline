import kwic.Data;
import kwic.Input;
import kwic.Shift;
import kwic.Sort;

import pipeline.Handler;
import pipeline.Pipeline;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Необходимо передать имя файла");
            return;
        }

        Data data = readData(args[0]);

        // Иницаилазция пайплайна
        Pipeline<Data> pipeline = new Pipeline<Data>(new Handler[]{
                new Input(),    // Ввод индекса
                new Shift(),    // Сдиг индекса
                new Sort(),     // Сортировка индекса
        });

        // Запуск пайплайна
        pipeline.Start(data);

        System.out.println(data);
    }


    private static Data readData(String filename) throws IOException {
        HashMap<Integer, String[]> pages = new HashMap<Integer, String[]>();

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int page = 1;
            ArrayList<String> lines = new ArrayList<>();
            for (String line; (line = br.readLine()) != null; ) {
                if (line.equals("#")) {
                    pages.put(page, lines.toArray(new String[0]));
                    page++;
                    lines = new ArrayList<>();
                    continue;
                }
                lines.add(line);
            }
            pages.put(page, lines.toArray(new String[0]));
        }
        return new Data(pages);
    }
}
