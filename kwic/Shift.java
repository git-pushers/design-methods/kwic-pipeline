package kwic;

import pipeline.Handler;

import java.util.ArrayList;

// Shift наполняет индекс элементами с учетом циклического сдвига
public class Shift implements Handler<Data> {

    public void Execute(Data d) {
        ArrayList<Line> shiftedIndex = new ArrayList<>();
        for (Line l : d.index) {
            for (int i = 0; i < l.words.length; i++) {
                shiftedIndex.add(new Line(l.page, l.line, i+1, l.words));
            }
        }
        d.index = shiftedIndex;
    }

}
