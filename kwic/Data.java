package kwic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Data {
    // Input
    HashMap<Integer, String[]> pages;

    // Calculated
    ArrayList<Line> index;

    public Data(HashMap<Integer, String[]> pages) {
        this.pages = pages;
    }

    public String toString() {
        String format = "%-80s %-30s %-20s %-20s\n";
        StringBuilder s = new StringBuilder();
        s.append(String.format(format, "Context", "Key", "Page", "Line"));
        for (Line l : index) {
            s.append(String.format(format, this.pages.get(l.page)[l.line - 1],
                    l.words[l.word - 1], l.page, l.line));
        }
        return s.toString();
    }
}

class Line {
    int word;
    int page;
    int line;
    String[] words;

    public Line(int page, int line, int word, String[] words) {
        this.page = page;
        this.line = line;
        this.word = word;
        this.words = words;
    }
}

