package kwic;

import pipeline.Handler;

import java.util.ArrayList;
import java.util.Map;

// Input заполняет первичный индекс
public class Input implements pipeline.Handler<Data> {

    @Override
    public void Execute(Data d) {
        d.index = new ArrayList<>();
        for (Map.Entry<Integer, String[]> entry : d.pages.entrySet()) {
            for (int i=0; i<entry.getValue().length; i++){
                d.index.add(new Line(entry.getKey(), i+1,1, toWords(entry.getValue()[i])));
            }
        }
    }

    private static String[] toWords(String line){
        return line.split(" ");
    }
}
