package kwic;

import pipeline.Handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

// Sort выполняет сортировку индекса
public class Sort implements Handler<Data> {
    public void Execute(Data d) {
        d.index.sort(Comparator.comparing(o -> o.words[o.word - 1]));
    }
}
